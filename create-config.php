<?php

$manifestfile = 'manifest';
$debug = FALSE;

function fetch_manifest($manifestfile) {
  GLOBAL $debug;
  if (function_exists('yaml_parse_file')) {
    if (file_exists($manifestfile)) {
      $manifest = yaml_parse_file($manifestfile);
      if ($debug) echo "manifest excuted with yaml_parse_file\n";
    }
  } else {
    echo 'use spyc';
    if (file_exists($manifestfile)) {
      include('gbdeploy/spyc.php');
      $manifest = Spyc::YAMLLoad($manifestfile);
      if ($debug) echo "manifest excuted with spyc.php\n";
    }
  }
  
  if ($debug) var_dump($manifest);
  
  return $manifest;
}

$manifest = fetch_manifest($manifestfile);
if ($debug) var_dump($manifest);
if ($debug) echo $manifest['databases']['prefix'];


$dbprefix = $manifest['databases']['prefix'];
$script = "/bin/bash wp-config.sh ".$dbprefix;
exec($script);
